package com.demo.appdatamx.materialtabsedgar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

/**
 * Created by EdgarGarcia on 15/02/2016.
 */
public class TabActivity extends ActionBarActivity implements MaterialTabListener {
    MaterialTabHost tabHost;
    ViewPager pager;
    ViewPagerAdapter adapter;
    Toolbar toolbar;
    RelativeLayout relativeTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
        pager = (ViewPager) this.findViewById(R.id.pager);
        relativeTabHost = (RelativeLayout) this.findViewById(R.id.relativeTabHost);

        // init view pager
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        relativeTabHost.setBackgroundResource(R.color.colorFragment01);
                        toolbar.setBackgroundResource(R.drawable.toolbar_01);
                        break;
                    case 1:
                        relativeTabHost.setBackgroundResource(R.color.colorFragment02);
                        toolbar.setBackgroundResource(R.drawable.toolbar_02);
                        break;
                    case 2:
                        relativeTabHost.setBackgroundResource(R.color.colorFragment03);
                        toolbar.setBackgroundResource(R.drawable.toolbar_03);
                        break;
                    case 3:
                        relativeTabHost.setBackgroundResource(R.color.colorFragment04);
                        toolbar.setBackgroundResource(R.drawable.toolbar_04);
                        break;
                    case 4:
                        relativeTabHost.setBackgroundResource(R.color.colorFragment05);
                        toolbar.setBackgroundResource(R.drawable.toolbar_05);
                        break;
                }
                // when user do a swipe the selected tab change
                tabHost.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // insert all tabs from pagerAdapter data
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(
                    tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );
        }
    }

    @Override
    public void onTabSelected(MaterialTab tab) {

        pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {

            super(fm);
        }

        @Override
        public Fragment getItem(int num) {
            switch (num) {
                case 0: return new fragment1();
                case 1: return new fragment2();
                case 2: return new fragment3();
                case 3: return new fragment4();
                case 4: return new fragment5();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Fragmento " + position;
        }

    }
}
